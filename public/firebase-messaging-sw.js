importScripts('https://www.gstatic.com/firebasejs/3.7.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.7.1/firebase-messaging.js');

// Initialize Firebase
  var config = {
    apiKey: "AIzaSyCtvsfMkW9KER9XeVEgCQu7xFbHvhmE6ro",
  	authDomain: "pwa-test-15c31.firebaseapp.com",
  	databaseURL: "https://pwa-test-15c31.firebaseio.com",
    storageBucket: "pwa-test-15c31.appspot.com",
  		messagingSenderId: "506368098139",
  };
  firebase.initializeApp(config);
  
  const messaging = firebase.messaging();