import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home
  },

  {
    path: '/panel-admin',
    name: 'panel-admin',
    component: () => import( '../views/Panel-admin.vue')
  },

  /*PROVIDERS*/
  {
    path: '/',
    name: 'Login Providers',
    component: () => import( '../views/providers-login.vue')
  },
  {
    path: '/providers/signup',
    name: 'Signup Providers',
    component: () => import( '../views/providers-signup.vue')
  },
  {
    path: '/providers/login',
    name: 'Login Providers',
    component: () => import( '../views/providers-login.vue')
  },
  {
    path: '/providers/add',
    name: 'Providers Add Product',
    component: () => import( '../views/providers-add.vue')
  },
  {
    path: '/providers/account',
    name: 'Providers account',
    component: () => import( '../views/providers-account.vue')
  },
  //ORDENES ENTRANTES
  {
    path: '/providers/orders',
    name: 'Providers orders',
    component: () => import( '../views/providers-orders.vue')
  },
  //HISTORIAL DE PEDIDOS 
  {
    path: '/providers/history',
    name: 'Providers history',
    component: () => import( '../views/providers-history.vue')
  },
  //DETALLES DE UN PEDIDO COMPLETO
  {
    path: '/providers/orders/:id',
    name: 'Provider detail order',
    component: () => import( '../views/detail-product.vue'),
    props: (route) => ({ query: route.query.q })
  },



  //HISTORIAL DE PAGOS
   {
    path: '/providers/payments',
    name: 'Providers payments',
    component: () => import( '../views/providers-payments.vue')
  },
  {
    path: '/providers/docs',
    name: 'Docs providers',
    component: () => import( '../views/providers-docs.vue')
  },
  {
    path: '/providers/sales',
    name: 'Sales providers',
    component: () => import( '../views/providers-sales.vue')
  },
  {
    path: '/providers/subscribe',
    name: 'Subscribe providers',
    component: () => import( '../views/providers-subcription.vue')
  },
  {
    path: '/recovery/:id',
    name: 'Recovery password',
    component: () => import( '../views/recovery-password.vue'),
    props: (route) => ({ query: route.query.q })
  },
  {
    path: '/reset/:id',
    name: 'Reset password',
    component: () => import( '../views/reset-password.vue'),
    props: (route) => ({ query: route.query.q })
  },
  {
    path: '/auth/:id',
    name: 'Autenticate',
    component: () => import( '../views/auth.vue'),
    props: (route) => ({ query: route.query.q })
  },
  //EDITAR PRODUCTOS
  {
    path: '/providers/edit/:id',
    name: 'Editar producto',
    component: () => import( '../views/providers-edit.vue'),
    props: (route) => ({ query: route.query.q })
  },




/***********************************/
  /*ADMIN*/
  {
    path: '/admin/login',
    name: 'Admin login',
    component: () => import( '../views/admin-login.vue')
  },

  {
    path: '/admin/home',
    name: 'Admin home',
    component: () => import( '../views/admin-home.vue')
  },
   {
    path: '/admin/management',
    name: 'Admin management',
    component: () => import( '../views/admin-management.vue')
  },
  {
    path: '/admin/history',
    name: 'Admin history',
    component: () => import( '../views/admin-history.vue')
  },
  /* REGISTRAR ESTABLECIMIENTO ADMIN*/
  {
    path: '/admin/add',
    name: 'Admin add',
    component: () => import( '../views/admin-add.vue')
  },

  /* ADMINISTRAR ORDENES*/
  {
    path: '/admin/orders',
    name: 'Admin orders',
    component: () => import( '../views/admin-orders.vue')
  },

  /* PAGOS A PROVEEDORES*/
  {
    path: '/admin/providers/payments',
    name: 'Admin providers payments',
    component: () => import( '../views/admin-providers-payments.vue')
  },
  /*PAGOS A REPARTIDORES*/ 
  {
    path: '/admin/delivery/payments',
    name: 'Admin delivery payments',
    component: () => import( '../views/admin-delivery-payments.vue')
  },
  /*REGISTRAR REPARTIDORES */
  {
    path: '/admin/delivery/add',
    name: 'Admin delivery add',
    component: () => import( '../views/admin-delivery-add.vue')
  },

  /*PAGINA CON TODOS LOS REPARTIDORES */
  {
    path: '/admin/delivery/',
    name: 'Admin delivery all',
    component: () => import( '../views/admin-delivery.vue')
  },
  {
    path: '/admin/delivery/edit/:id',
    name: 'Admin delivery edit',
    component: () => import( '../views/admin-delivery-edit.vue'),
    props: (route) => ({ query: route.query.q })
  },

  //EDITAR COMERCIO ADMIN
  {
    path: '/admin/providers/edit/:id',
    name: 'Admin edit provider',
    component: () => import( '../views/admin-providers-edit.vue'),
    props: (route) => ({ query: route.query.q })
  },
  //EDITAR ADMIN ADMIN
  {
    path: '/admin/admin/edit/:id',
    name: 'Admin edit admin',
    component: () => import( '../views/admin-admin-edit.vue'),
    props: (route) => ({ query: route.query.q })
  },
  //AÑADIR ADMIN ADMIN
  {
    path: '/admin/admin/add/',
    name: 'Admin add admin',
    component: () => import( '../views/admin-admin-add.vue'),
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
